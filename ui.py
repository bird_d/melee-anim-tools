from bpy.types import Panel, UIList, Menu
import textwrap
import bpy


class VIEW3D_PT_melee_tools(Panel):
    """Creates a Panel in the Graph editor N-Panel"""
    bl_category = "MELEE"
    bl_label = "Melee Anim Tools"
    bl_idname = "VIEW3D_PT_melee_tools"
    bl_options = {'HIDE_HEADER'}
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    
    def draw(self, context):
        draw_panel(self.layout, context)

def draw_panel(layout, context):
    melee = context.scene.brd_melee
    obj = melee.armature
    
    # Begin drawing UI
    col = layout.column(align=False)
    box = col.box()
    box.prop(melee, "export_path", text="Export Path")
    col = box.column(align=True)
    col.prop(melee, "export_prefix", text="Prefix")
    col.prop(melee, "export_suffix", text="Suffix")
    
    if len(melee.animations) > 0 and melee.animations[melee.active_anim].action:
        col.label(text="%s%s%s" % (melee.export_prefix, melee.animations[melee.active_anim].action.name, melee.export_suffix))
    else:
        col.label(text="")

    # Armature picker
    row = layout.row(align=True)
    row.prop(melee, "sync_preview_range", text="", icon='UV_SYNC_SELECT')
    row.prop(melee, "armature", text="")

    # Sync preview range button
    
    # List area
    row = layout.row()
    row.template_list("NLATRACKS_UL_layer_list", "", melee, "animations", melee, "active_anim")
    
    col = row.column(align=True)
    col.operator("brd_melee.add_animation", text="", icon='ADD')
    col.operator("brd_melee.remove_animation", text="", icon='REMOVE')

    col = col.column()
    col.separator()
    col.menu("MESH_MT_melee_anim_context_menu", icon='DOWNARROW_HLT', text="")

    
    # Export buttons
    col = layout.column(align=False)
    col.operator("brd_melee.export_all", text="Export All", icon='EXPORT')
    row.split()
    
    # Errors/Warnings
    error = []
    if obj:
        for bone in obj.data.bones:
            if bone.use_local_location:
                error.append(("'Local Location' enabled", "brd_melee.fix_local_location_error"))
                break

        action = melee.animations[melee.active_anim].action if len(melee.animations) > 0 else None
        if action:
            if obj.data.bones[0].name in action.groups:
                for channel in action.groups[obj.data.bones[0].name].channels:
                    if channel.data_path.split('.')[-1] in ["rotation_euler", 
                                                            "rotation_axis_angle", 
                                                            "rotation_quaternian"]:
                        error.append(("Root has rotation keys", "brd_melee.fix_root_rotation_error"))
                        break

    col = layout.column(align=True)
    err_len = len(error)
    if err_len > 0:
        box = col.box()
        box.label(text='%d Warning%s' % (err_len, 's' if err_len > 1 else ''), icon='ERROR')
        box = col.box()
        for err in error:
            row = box.row(align=True)
            row.label(text=err[0])
            row.operator(err[1], text='', icon='MODIFIER')

#How to draw the layer's list
class NLATRACKS_UL_layer_list(UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        preferences = context.preferences.addons[__package__].preferences
        self.use_filter_sort_reverse = False
        anim = item
        if self.layout_type in {'DEFAULT', 'COMPACT'}:
            if anim:
                row = layout.row(align=True)

                if context.scene.brd_melee.animations[context.scene.brd_melee.active_anim] == anim:
                    row.label(text="", icon='KEYFRAME_HLT')
                else:
                    row.label(text="", icon='KEYFRAME')

                row.prop(anim, "action", text="", emboss=True)
                row.prop(anim, "framecount", text="", emboss=True)

                if anim.action:
                    op = row.operator("brd_melee.export_single", text="", icon='EXPORT')
                    op.index = index
                else:
                    op = row.operator("brd_melee.add_new_action", text="", icon='DUPLICATE')
                    op.index = index
            else:
                layout.label(text="", translate=False, icon_value=icon)
        elif self.layout_type in {'GRID'}:
            layout.alignment = 'CENTER'
            layout.label(text="", icon_value=icon)

    def draw_filter(self, context, layout):
        row = layout.row(align=True)
        row.prop(self, 'filter_name', text='', icon='VIEWZOOM')
        row.prop(self, 'use_filter_invert', text='', icon='ARROW_LEFTRIGHT')

class MESH_MT_melee_anim_context_menu(Menu):
    bl_label = "Melee Anim Specials"

    def draw(self, _context):
        layout = self.layout
        layout.operator("brd_melee.action_local_to_global")
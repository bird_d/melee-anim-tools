import bpy
from bpy.types import Operator
from bpy.path import abspath
from os import path, mkdir
from . import types
from .blender_maya import GenerateAnimFile
from mathutils import Matrix, Vector


class BRD_MELEE_OT_select_action(Operator):
    """Select an action to use"""
    bl_idname = "brd_melee.select_action"
    bl_label = "Select action"
    bl_options = {'REGISTER'}

    def invoke(self, context, event):
        scene = context.scene
        melee = scene.brd_melee

        obj = melee.armature if melee.armature else context.active_object
        if obj == None or obj.type != 'ARMATURE':
            self.report({'WARNING'}, "Please choose a target armature or select one")
            return {'CANCELLED'}
            
        action = melee.animations[melee.active_anim].action
        if action == None:
            self.report({'WARNING'}, "Melee animation has no action selected")
            return {'CANCELLED'}
            
        if obj.animation_data.action == action:
            return {'CANCELLED'}
            
        # Clear bone transforms in case some actions don't have keyframes
        for bone in obj.pose.bones:
            bone.location = (0,0,0)
            bone.rotation_euler = (0,0,0)
            bone.scale = (1,1,1)
        
        if melee.sync_preview_range:
            scene.frame_preview_start = scene.frame_start
            scene.frame_preview_end = scene.frame_start + melee.animations[melee.active_anim].framecount - 1
        
        obj.animation_data.action = action
        return {'FINISHED'}


class BRD_MELEE_OT_add_animation(Operator):
    """Add an animation setup for export using melee tools"""
    bl_idname = "brd_melee.add_animation"
    bl_label = "Add animation"
    bl_options = {'REGISTER', 'UNDO'}

    def invoke(self, context, event):
        context.scene.brd_melee.animations.add()
        return {'FINISHED'}


class BRD_MELEE_OT_remove_animation(Operator):
    """Remove an animation setup"""
    bl_idname = "brd_melee.remove_animation"
    bl_label = "Remove animation setup"
    bl_options = {'REGISTER', 'UNDO'}

    def invoke(self, context, event):
        brd_melee = context.scene.brd_melee
        brd_melee.animations.remove(brd_melee.active_anim)
        brd_melee.active_anim -= 1
        return {'FINISHED'}


class BRD_MELEE_OT_add_new_action(Operator):
    """Add an action and immediately link it to an animation setup"""
    bl_idname = "brd_melee.add_new_action"
    bl_label = "Create new action for animation setup"
    bl_options = {'REGISTER', 'UNDO'}

    index : bpy.props.IntProperty()

    def invoke(self, context, event):
        anim = context.scene.brd_melee.animations[self.index]
        action = bpy.data.actions.new("Anim")
        action.use_fake_user = True
        anim.action = action

        return {'FINISHED'}


class BRD_MELEE_OT_export_all(Operator):
    """Export all animations as selected format"""
    bl_idname = "brd_melee.export_all"
    bl_label = "Export all animations"
    bl_options = {'REGISTER'}
        
    def invoke(self, context, event):
        melee = context.scene.brd_melee
        ex_path = abspath(melee.export_path)
        if not path.isdir(ex_path):
            mkdir(ex_path)

        for anim in melee.animations:
            if anim.action == None:
                continue
            file_path = path.join(ex_path, melee.export_prefix + anim.action.name + melee.export_suffix + ".anim")
            GenerateAnimFile(context, anim.action, context.scene.frame_start, context.scene.frame_start + anim.framecount - 1).save_file(file_path)
        self.report({"INFO"}, "Exported all anims to: %s" % ex_path)
        return {'FINISHED'}


class BRD_MELEE_OT_export_single(Operator):
    """Export single animation as selected format"""
    bl_idname = "brd_melee.export_single"
    bl_label = "Export single animation"
    bl_options = {'REGISTER'}

    index : bpy.props.IntProperty()

    def invoke(self, context, event):
        melee = context.scene.brd_melee
        anim = melee.animations[self.index]
        ex_path = abspath(melee.export_path)

        if not path.isdir(ex_path):
            mkdir(ex_path)

        file_path = path.join(ex_path, melee.export_prefix + anim.action.name + melee.export_suffix + ".anim")
        GenerateAnimFile(context, anim.action, context.scene.frame_start, context.scene.frame_start + anim.framecount - 1).save_file(file_path)
        self.report({"INFO"}, "Exported anim, %s, to: %s" % (anim.action.name, file_path))
        return {"FINISHED"}


class BRD_MELEE_OT_fix_local_location_error(Operator):
    """Armature has bones that have 'Local Location' enabled, this will cause problems with animation when exporting. 
If you choose to use this fix, there is an option to fix animations in the menu below the "remove animation" button
    """
    bl_idname = "brd_melee.fix_local_location_error"
    bl_label = "Fix Local Location bones for target armature"
    bl_options = {'REGISTER', 'UNDO'}

    def invoke(self, context, event):
        melee = context.scene.brd_melee
        obj = melee.armature

        if obj == None or obj.type != 'ARMATURE':
            self.report({'WARNING'}, "Please choose a target armature or select one")
            return {'CANCELLED'}

        for bone in obj.data.bones:
            bone.use_local_location = False    
        return {'FINISHED'}


class BRD_MELEE_OT_fix_root_rotation_error(Operator):
    """Current animation has keyframes for rotation, this may cause the animation to rotate when exporting"""
    bl_idname = "brd_melee.fix_root_rotation_error"
    bl_label = "Fix root rotation in animation"
    bl_options = {'REGISTER', 'UNDO'}

    def invoke(self, context, event):
        melee = context.scene.brd_melee
        obj = melee.armature if melee.armature else context.active_object

        if obj == None or obj.type != 'ARMATURE':
            self.report({'WARNING'}, "Please choose a target armature or select one")
            return {'CANCELLED'}

        action = melee.animations[melee.active_anim].action
        if action:
            for fcurve in action.fcurves:
                if not fcurve.data_path.startswith('pose.bones["%s"]' % obj.data.bones[0].name):
                    continue
                if fcurve.data_path.split('.')[-1] in ["rotation_euler", "rotation_axis_angle", "rotation_quaternian"]:
                    action.fcurves.remove(fcurve)
            return {'FINISHED'}
        else:
            return {'CANCELLED'}


class BRD_MELEE_OT_action_local_to_global(Operator):
    """Converts the active animation to have global location translations from local"""
    bl_idname = "brd_melee.action_local_to_global"
    bl_label = "Convert Action Local Location to Global"
    bl_options = {'REGISTER', 'UNDO'}

    def invoke(self, context, event):
        melee = context.scene.brd_melee
        obj = melee.armature if melee.armature else context.active_object

        if obj == None or obj.type != 'ARMATURE':
            self.report({'WARNING'}, "Please choose a target armature or select one")
            return {'CANCELLED'}

        action = melee.animations[melee.active_anim].action

        if action == None:
            return {'CANCELLED'}

        for group in action.groups:
            bone = obj.data.bones[group.name]
            
            mtx = bone.matrix
            
            if bone == None:
                continue
            
            # Build Translation channel list and add if they don't exist
            t_channels = {}
            axis = ['X', 'Y', 'Z']
            for channel in group.channels:
                if channel.data_path.split(".")[-1] == "location":
                    t_channels[axis[channel.array_index]] = channel
            
            if len(t_channels) == 0:
                continue

            for i in range(0, len(axis)):
                if axis[i] not in t_channels:
                    fcurve = action.fcurves.new(
                        data_path='pose.bones["%s"].location' % group.name,
                        index=i,
                        action_group=group.name)
                    t_channels[axis[i]] = fcurve
            
            # Translate keyframes, making sure to check each channel in case 
            #   they have keyframes other channels don't
            fixed_keyframes = set()
            for axis in t_channels:
                channel = t_channels[axis]
                for i in range(0, len(channel.keyframe_points)):
                    kp = channel.keyframe_points[i]
                    co = kp.co
                    if co[0] in fixed_keyframes:
                        continue
                    
                    frame = co[0]
                    
                    xc, yc, zc = (
                        t_channels["X"],
                        t_channels["Y"],
                        t_channels["Z"])
                    
                    loc = mtx @ Vector((
                        xc.evaluate(frame),
                        yc.evaluate(frame),
                        zc.evaluate(frame)))
                        
                    xc.keyframe_points.insert(
                        frame=co[0],
                        value=loc[0])
                    yc.keyframe_points.insert(
                        frame=co[0],
                        value=loc[1])
                    zc.keyframe_points.insert(
                        frame=co[0],
                        value=loc[2])
                    
                    fixed_keyframes.add(co[0])

        for bone in obj.data.bones:
            bone.use_local_location = False
        
        return {'FINISHED'}
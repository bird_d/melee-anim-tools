# Melee Anim Tools, Adds tools to help with the process of exporting 
# ssbm animations from blender.
# Copyright (C) 2021 bird_d <relay198@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

bl_info = {
    "name": "Melee anim tools",
    "author": "bird_d",
    "version": (0, 1, 0),
    "blender": (2, 91, 0),
    "location": "3D View > N-Panel > Melee",
    "description": "Adds some tools to make exporting melee animations a little quicker",
    "warning": "",
    "doc_url": "",
    "category": "Animation",
}

if "bpy" in locals():
    import importlib
    importlib.reload(operators)
    importlib.reload(ui)
    importlib.reload(types)
    importlib.reload(blender_maya)
else:
    from . import operators
    from . import ui
    from . import types
    from . import blender_maya

import bpy
from bpy.props import IntProperty, BoolProperty
from bpy.types import AddonPreferences



class MeleeAnimToolsSettings(AddonPreferences):
    # this must match the addon name, use '__package__'
    # when defining this in a submodule of a python package.
    bl_idname = __name__
    # show_action_in_layers : BoolProperty(default=True, description="Shows each layer's action in the layer list")

    def draw(self, context):
        layout = self.layout.box()
        #layout.prop(self, "show_action_in_layers", text="Show Actions")


#region    #Msgbus#

def on_active_anim_change(*args):
    bpy.ops.brd_melee.select_action('INVOKE_DEFAULT')

def on_active_anim_framecount_change(*args):
    brd_melee = bpy.context.scene.brd_melee
    anim = brd_melee.animations[brd_melee.active_anim]
    scene = bpy.context.scene
    scene.frame_preview_start = scene.frame_start
    scene.frame_preview_end = scene.frame_start + anim.framecount

nla_subscription_owner = object()

#Make it so we call a function when subscribe_to's data path updates
def subscribe_properties(subscription_owner):
    subscribe_to = types.MeleeScene, "active_anim"
    bpy.msgbus.subscribe_rna(
        key=subscribe_to,
        owner=subscription_owner,
        args=(),
        notify=on_active_anim_change,
        options={'PERSISTENT',}
    )

    subscribe_to = types.MeleeAnim, "framecount"
    bpy.msgbus.subscribe_rna(
        key=subscribe_to,
        owner=subscription_owner,
        args=(),
        notify=on_active_anim_framecount_change,
        options={'PERSISTENT',}
    )

    if load_handler not in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.append(load_handler)

def unsubscribe_properties(subscription_owner):
    if subscription_owner is not None:
        bpy.msgbus.clear_by_owner(subscription_owner)

    if load_handler in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.remove(load_handler)

#Make it so everything is resubscribed if loading a new file
from bpy.app.handlers import persistent
@persistent
def load_handler(dummy):
    subscribe_properties(nla_subscription_owner)

#endregion #Msgbus#

#region    #Registration#
classes = [
    MeleeAnimToolsSettings,
    # types.py
    types.MeleeAnim,
    types.MeleeScene,
    #UI
    ui.VIEW3D_PT_melee_tools,
    ui.NLATRACKS_UL_layer_list,
    ui.MESH_MT_melee_anim_context_menu,
    #Operators
    operators.BRD_MELEE_OT_select_action,
    operators.BRD_MELEE_OT_add_animation,
    operators.BRD_MELEE_OT_remove_animation,
    operators.BRD_MELEE_OT_export_all,
    operators.BRD_MELEE_OT_export_single,
    operators.BRD_MELEE_OT_fix_local_location_error,
    operators.BRD_MELEE_OT_fix_root_rotation_error,
    operators.BRD_MELEE_OT_add_new_action,
    operators.BRD_MELEE_OT_action_local_to_global,
]

addon_keymaps = []

def register():
    try:
        unregister()
    except:
        pass
        
    #Register classes
    for cls in classes:
        bpy.utils.register_class(cls)
    
    bpy.types.Scene.brd_melee = bpy.props.PointerProperty(type=types.MeleeScene)

    #Msgbus
    subscribe_properties(nla_subscription_owner)

def unregister():

    #Unregister classes
    for cls in classes:
        bpy.utils.unregister_class(cls)

    #Msgbus
    unsubscribe_properties(nla_subscription_owner)

#endregion #Registration#

if __name__ == "__main__":
    try:
        unregister()
    except RuntimeError as e:
        print(e)
    except AttributeError as e:
        print(e)
    except:
        import sys
        print("Error:", sys.exc_info()[0])
        pass
    register()
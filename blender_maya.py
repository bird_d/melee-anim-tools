# Taken from the m-ex discord and modified slightly for batch exporting.
# Did not come with a license, and shouldn't be considered to be 
#   under the license included with this repo

import math
import os
import re
import xml.etree.ElementTree as ET
from math import atan2, ceil, cos, degrees, floor, isclose, pi, radians, sin,tan

import bpy
from bpy.props import BoolProperty, EnumProperty, IntProperty, StringProperty,CollectionProperty
from bpy.types import Operator, OperatorFileListElement
from bpy_extras.io_utils import ExportHelper, ImportHelper
from mathutils import Euler, Matrix, Quaternion, Vector

bl_info = {
    "name": "Import/Export Maya .anim",
    "author": "Ploaj",
    "version": (0, 1),
    "blender": (2, 80, 0),
    "location": "File > Export",
    "description": "Import/Export Maya Animation Format (.anim)",
    "warning": "",
    "doc_url": "",
    "support": 'OFFICIAL',
    "category": "Import-Export",
}

#todo: support angular degree
#todo: support quaternion track
#todo: bake and simply track option

def set_active_object(context,obj):
    context.view_layer 
    
__bl_classes = []
def register_wrap(cls):
    if hasattr(cls, 'bl_rna'):
        __bl_classes.append(cls)
    return cls

class ANIMFile:
    def __init__(self):
        self.header = ANIMHeader()
        self.animnodes = []
    
    def parse_file(self, filePath):
        with open(filePath, "r") as f:
            current_track = 0
            while True:
                args = f.readline().strip().replace(";", "").split(' ')
                
                if args[0] == "":
                    break
                
                if args[0] == "animVersion":
                    self.header.anim_version = args[1]
                elif args[0] == "mayaVersion":
                    self.header.maya_version = args[1]
                elif args[0] == "timeUnit":
                    self.header.time_unit = args[1]
                elif args[0] == "linearUnit":
                    self.header.linear_unit = args[1]
                elif args[0] == "angular_unit":
                    self.header.angular_unit = args[1]
                elif args[0] == "startTime":
                    self.header.start_time = int(args[1])
                elif args[0] == "endTime":
                    self.header.end_time = int(args[1])
                elif args[0] == "anim":
                    nodeName = ""
                    
                    # Parse node name
                    if len(args) == 7:
                        nodeName = args[3]
                    else:
                        nodeName = args[1]
                    
                    # Find node
                    current_node = 0
                    for node in self.animnodes:
                        if node.name == nodeName:
                            current_node = node
                            break
                    
                    # Node wasn't found so add it
                    if current_node == 0:
                        current_node = ANIMNode()
                        current_node.name = nodeName
                        self.animnodes.append(current_node)
                    
                    # Begin new track
                    current_track = ANIMTrack()
                    
                    if len(args) == 7:
                        current_track.track_type = args[2]
                        current_node.animtracks.append(current_track)
                        
                elif args[0] == "animData":
                    if current_track == 0:
                        continue
                    
                    data_line = f.readline()
                    
                    while not "}" in data_line:
                        data_args = data_line.strip().replace(";", "").split(' ')
                        
                        if data_args[0] == "input":
                            current_track.input = data_args[1]
                        elif data_args[0] == "output":
                            current_track.output = data_args[1]
                        elif data_args[0] == "weighted":
                            current_track.weighted = data_args[1] == "1"
                        elif data_args[0] == "preInfinity":
                            current_track.pre_infinity = data_args[1]
                        elif data_args[0] == "postInfinity":
                            current_track.post_infinity = data_args[1]
                        elif data_args[0] == "keys":
                            key_line = f.readline()
                            while not "}" in key_line:
                                key_args = key_line.strip().replace(";", "").split(" ")
                                
                                key = ANIMKey()
                                key.input = float(key_args[0])
                                key.output = float(key_args[1])
                                
                                if len(key_args) >= 7:
                                    key.intan = key_args[2]
                                    key.outtan = key_args[3]
                                
                                if len(key_args) > 8:
                                    key.t1 = float(key_args[7])
                                    key.w1 = float(key_args[8])
                                
                                if len(key_args) > 10:
                                    key.t2 = float(key_args[9])
                                    key.w2 = float(key_args[10])
                                
                                if float(key.input) <= float(self.header.end_time):
                                    current_track.animkeys.append(key)
                                key_line = f.readline()
                        data_line = f.readline()
                    
    def save_file(self, filePath):
        with open(filePath, "w") as f:
            f.write("animVersion {};\n".format(self.header.anim_version))
            f.write("mayaVersion {};\n".format(self.header.maya_version))
            f.write("timeUnit {};\n".format(self.header.time_unit))
            f.write("linearUnit {};\n".format(self.header.linear_unit))
            f.write("angularUnit {};\n".format(self.header.angular_unit))
            f.write("startTime {};\n".format(self.header.start_time))
            f.write("endTime {};\n".format(self.header.end_time))
            
            row = 0
            
            for node in self.animnodes:
                track_index = 0
                if len(node.animtracks) == 0:
                    f.write("anim {} 0 1 {};\n".format(node.name, track_index))
                    track_index += 1
                for track in node.animtracks:
                    f.write("anim {}.{} {} {} 0 1 {};\n".format(track.get_control_type(), track.track_type, track.track_type, node.name, track_index))
                    track_index += 1
                    
                    f.write("animData {\n")
                    f.write(" input {};\n".format(track.input))
                    f.write(" output {};\n".format(track.output))
                    f.write(" weighted {};\n".format("1" if track.weighted else "0"))
                    f.write(" preInfinity {};\n".format(track.pre_infinity))
                    f.write(" postInfinity {};\n".format(track.post_infinity))
                    
                    f.write(" keys {\n")
                    for key in track.animkeys:
                        tanin = " {} {}".format(key.t1, key.w1) if key.intan == "spline" or key.intan == "fixed" or key.intan == "auto" else ""
                        tanout = " {} {}".format(key.t2, key.w2) if key.outtan == "spline" or key.outtan == "fixed" or key.outtan == "auto" else ""
                        f.write(" {} {} {} {} 1 1 0{}{};\n".format(key.input, key.output, key.intan, key.outtan, tanin, tanout))
                    f.write(" }\n")
                    
                    f.write("}\n")

class ANIMHeader:
    def __init__(self):
        self.anim_version = 1.1
        self.maya_version = 2015
        self.start_time = 1
        self.end_time = 1
        self.start_unitless = 0
        self.end_unitless = 0
        self.time_unit = "ntscf"
        self.linear_unit = "cm"
        self.angular_unit = "deg"
        
class ANIMKey:
    def __init__(self):
        self.input = 0
        self.output = 0
        self.intan = "linear"
        self.outtan = "linear"
        self.t1 = 0
        self.w1 = 1
        self.t2 = 0
        self.w2 = 1
        
class ANIMTrack:
    def __init__(self):
        self.track_type = "translateX"
        self.input = "time"
        self.output = "linear"
        self.pre_infinity = "constant"
        self.post_infinity = "constant"
        self.weighted = False
        self.animkeys = []
    
    def get_component_index(self):
        if self.track_type.endswith("X"):
            return 0
        elif self.track_type.endswith("Y"):
            return 1
        elif self.track_type.endswith("Z"):
            return 2
        elif self.track_type.endswith("W"):
            return 3
        else:
            return 0
    
    def get_control_type(self):
        if "translate" in self.track_type:
            return "translate"
        if "rotate" in self.track_type:
            return "rotate"
        if "scale" in self.track_type:
            return "scale"
        if "visibility" in self.track_type:
            return "visibility"

class ANIMNode:
    def __init__(self):
        self.name = ""
        self.animtracks = []
    

def GenerateAnimFile(context, action, frame_start, frame_end):

    axis = ['X','Y','Z','W']
    exp = re.compile(r".+\.([^\.]+)$")
    track_type = {'location' : 'translate', 'rotation_euler' : 'rotate', 'rotation_quaternion' : 'rotate', 'scale':'scale'}
    intpl_type = {'CONSTANT' : 'step', 'LINEAR' : 'linear', 'BEZIER' : 'spline'}
    
    dict = {}

    for group in action.groups:
        node = ANIMNode()
        node.name = group.name
        dict[group.name] = node
        for channel in group.channels:
            data_component = exp.sub(r'\1',channel.data_path)
            track = ANIMTrack()
            track.track_type = "{}{}".format(track_type[data_component], axis[channel.array_index])
            track.pre_infinity = channel.extrapolation.lower()
            track.post_infinity = channel.extrapolation.lower()
            
            if track.get_control_type() == "rotate":
                track.output = "angular"
                
            node.animtracks.append(track)
            
            bone = context.object.data.bones[node.name]
            if bone.parent:
                mtx = bone.parent.matrix_local.inverted() @ bone.matrix_local
            else:
                mtx = bone.matrix_local
            tra, rot, sca = mtx.decompose()
            eul = rot.to_euler()
        
            for key_info in channel.keyframe_points:
                frame = key_info.co[0]
                value = key_info.co[1]
                
                handle_left = key_info.handle_left
                offset_left = (frame - handle_left[0], value - handle_left[1])
                angle_left = degrees(atan2(offset_left[1],offset_left[0]))
                
                handle_right = key_info.handle_right
                offset_right = (handle_right[0] - frame,  handle_right[1] - value)
                angle_right = degrees(atan2(offset_right[1],offset_right[0]))
                
                if not key_info.interpolation in intpl_type:
                    continue
                
                if track.get_control_type() == "translate":
                    value += tra[track.get_component_index()]
                if track.get_control_type() == "rotate":
                    value += eul[track.get_component_index()]
                if track.get_control_type() == "scale":
                    value /= sca[track.get_component_index()]
                
                key = ANIMKey()
                key.input = int(frame)
                key.output = value
                key.intan = intpl_type[key_info.interpolation]
                key.outtan = intpl_type[key_info.interpolation]
                key.t1 = angle_left 
                key.t2 = angle_right 
                track.animkeys.append(key)
    
    animFile = ANIMFile()
    animFile.header.start_time = frame_start
    animFile.header.end_time = frame_end
    animFile.header.angular_unit = "rad"
    
    bones = [b.name for b in context.object.data.bones]
    for b in bones:
        if b in dict:
            animFile.animnodes.append(dict[b])
        else:
            node = ANIMNode()
            node.name = b
            animFile.animnodes.append(node)
    
    return animFile

def ImportAnimFile(context, filePath):
    file = ANIMFile()
    file.parse_file(filePath)
    anim_name = "new_anim"
    
    ResetScene(context)
    
    action = context.blend_data.actions.new(anim_name)

    if context.active_object.animation_data is None:
        context.active_object.animation_data_create()

    context.active_object.animation_data.action = action

    context.scene.frame_preview_start = file.header.start_time
    context.scene.frame_preview_end = file.header.end_time
    context.scene.use_preview_range = True
    
    track_type = {
        'translate' : 'location', 
        'rotate' : 'rotation_euler',
        'scale':'scale'
    }
    intpl_type = {
        'step' : 'CONSTANT', 
        'linear' : 'LINEAR', 
        'spline' : 'BEZIER',
        'fixed' : 'BEZIER',
        'auto' : 'BEZIER'
    }
    
    bones = [b.name for b in context.object.data.bones]
    
    for node in file.animnodes:
        
        if node.name in bones and node.name not in action.groups:
            anim_group = action.groups.new(node.name)
            anim_group.name = node.name
        else:
            continue
        
        bone = context.object.data.bones[node.name]
        if bone.parent:
            mtx = bone.parent.matrix_local.inverted() @ bone.matrix_local
        else:
            mtx = bone.matrix_local
        tra, rot, sca = mtx.decompose()
        eul = rot.to_euler()
        
        pose_bone = context.object.pose.bones[node.name]
        pose_bone.rotation_mode = 'XYZ'
        
        for track in node.animtracks:
            curve = action.fcurves.new("pose.bones[\"{}\"].{}".format(node.name, track_type[track.get_control_type()]), index = track.get_component_index(), action_group = action.groups[node.name].name)
            
            i = -1
            for key in track.animkeys:
                kf = curve.keyframe_points.insert(key.input, key.output)
                
                if track.get_control_type() == "translate":
                    kf.co[1] -= tra[track.get_component_index()]
                if track.get_control_type() == "rotate":
                    kf.co[1] -= eul[track.get_component_index()]
                if track.get_control_type() == "scale":
                    kf.co[1] *= sca[track.get_component_index()]
                    
                kf.interpolation = intpl_type[key.outtan]
                
                i += 1
                
                if kf.interpolation == "BEZIER":
                    handle_left_angle = key.t1
                    handle_right_angle = key.t2
                    weight_left = key.w1
                    weight_right = key.w2
                    
                    handle_left_offset =  Vector((1, tan(handle_left_angle * pi / 180)  ))
                    handle_right_offset = Vector((1, tan(handle_right_angle * pi / 180)  ))
                    
                    handle_left_offset = handle_left_offset.normalized() * weight_left
                    handle_right_offset = handle_right_offset.normalized() * weight_right
                    
                    co = kf.co
                    
                    if key.intan == "auto" and key.outtan == "auto":
                        kf.handle_left_type = "AUTO"
                    else:
                        kf.handle_left_type = "FREE"
                        kf.handle_left = (co[0] - handle_left_offset[0], co[1] - handle_left_offset[1])
               
                    if key.intan == "auto" and key.outtan == "auto":
                        kf.handle_right_type = "AUTO"
                    else:
                        kf.handle_right_type = "FREE"
                        kf.handle_right = (co[0] + handle_right_offset[0], co[1] + handle_right_offset[1])
            
            # hsd does not use variable weights but instead has set ones
            # based on the distance between keys
            for i in range(0,len(curve.keyframe_points)):
                keyframe = curve.keyframe_points[i]
                co = keyframe.co 

                if keyframe.handle_left_type != 'AUTO' and i > 0:
                    keyframe_left = curve.keyframe_points[i-1]
                    offset = abs(co[0] - keyframe_left.co[0]) 
                    keyframe.handle_left = (co[0] - (1.0/3.0) * offset ,co[1] + (keyframe.handle_left[1] - co[1]) * offset/3.0 ) 
                if  keyframe.handle_right_type != 'AUTO' and i < len(curve.keyframe_points) - 1:
                    keyframe_right = curve.keyframe_points[i+1]
                    offset = abs(co[0] - keyframe_right.co[0])
                    keyframe.handle_right = (keyframe_right.co[0] - (2.0/3.0) * (keyframe_right.co[0] - co[0]),co[1] + (keyframe.handle_right[1] - co[1]) * offset/3.0)
                    
                    
def ResetScene(context):
    prev_auto = context.scene.tool_settings.use_keyframe_insert_auto
    context.scene.tool_settings.use_keyframe_insert_auto = False
    
    bpy.ops.object.mode_set(mode='POSE')
    
    bpy.ops.pose.select_all(action='SELECT')
    bpy.ops.pose.loc_clear()
    bpy.ops.pose.rot_clear()
    bpy.ops.pose.scale_clear()
    
    context.scene.tool_settings.use_keyframe_insert_auto = prev_auto

@register_wrap
class POSE_OT_maya_anim_import(Operator, ImportHelper):
    bl_idname = "maya.anim_import"
    bl_label = "Maya .Anim Import"
    
    files : CollectionProperty(
        name="File Path",
        type=OperatorFileListElement,
        )
    directory : StringProperty(
            subtype='DIR_PATH',
            )
    # ExportHelper mixin class uses this
    filename_ext = ".anim"

    filter_glob : StringProperty(
            default="*.anim",
            options={'HIDDEN'},
            maxlen=255,  # Max internal buffer length, longer would be clamped.
            )

    @classmethod
    def poll(cls, context):
        return (context.active_object is not None) and isinstance(context.active_object.data, bpy.types.Armature)

    def execute(self, context):
        
        directory = self.directory
        filepaths = [os.path.join(directory, file_elem.name) for file_elem in self.files if os.path.isfile(os.path.join(directory, file_elem.name))]
        
        for filepath in filepaths:
            ImportAnimFile(context, filepath)
            
        return {'FINISHED'}

@register_wrap
class POSE_OT_maya_anim_export(Operator, ExportHelper):
    bl_idname = "maya.anim_export"
    bl_label = "Maya .Anim Export"

    filename_ext =  ".anim"
    
    filter_glob : StringProperty(
            default="*.anim",
            options={'HIDDEN'},
            maxlen=255,  # Max internal buffer length, longer would be clamped.
            )

    @classmethod
    def poll(cls, context):
        return (context.active_object is not None) and isinstance(context.active_object.data, bpy.types.Armature) and (context.active_object.animation_data is not None) and (context.active_object.animation_data.action is not None)

    def execute(self, context):
        GenerateAnimFile(context, None).save_file(self.filepath)
        return {'FINISHED'}

    def invoke(self, context, event):
        self.filepath = context.active_object.animation_data.action.name
        wm = context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


def menu_func_import(self, context):
    self.layout.operator(POSE_OT_maya_anim_import.bl_idname,text='Maya Anim (.anim)')


def menu_func_export(self, context):
    self.layout.operator(POSE_OT_maya_anim_export.bl_idname,text='Maya Anim (.anim)')


def register(): 
    from bpy.utils import register_class
    for cls in __bl_classes:
        register_class(cls)
        
    #bpy.utils.register_class(POSE_OT_maya_anim_export) 
    bpy.types.TOPBAR_MT_file_import.append(menu_func_import)
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)
    
        
def unregister(): 
    #bpy.utils.unregister_class(POSE_OT_maya_anim_export) 
    bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)
    
    from bpy.utils import unregister_class
    for cls in __bl_classes:
        unregister_class(cls)

#unregister()
if __name__ == "__main__": 
    register()
import bpy
from bpy.props import (
    BoolProperty,
    CollectionProperty, 
    IntProperty,
    PointerProperty,
    StringProperty
    )

from bpy.types import PropertyGroup, Action

def anim_action_prop_poll(self, action):
    for anim in bpy.context.scene.brd_melee.animations:
        if action == anim.action:
            return False
    return True

class MeleeAnim(PropertyGroup):
    action : PointerProperty(type=Action, poll=anim_action_prop_poll)
    framecount : IntProperty(default=60, min=1)

def melee_scene_armature_prop_poll(self, obj):
    if obj.type != 'ARMATURE':
        return False
    return True

class MeleeScene(PropertyGroup):
    armature : PointerProperty(type=bpy.types.Object, poll=melee_scene_armature_prop_poll)
    export_path : StringProperty(name="Export Path", default='//anim_export')
    export_prefix : StringProperty(name="Export Prefix")
    export_suffix : StringProperty(name="Export Suffix", default="_figatree")
    
    active_anim : IntProperty(default=0)
    
    sync_preview_range : BoolProperty(
        name="Sync Preview Range to Framecount", 
        description="Keep the scene's preview range synced to the framecount of the selected animation", 
        default=True)
    animations : CollectionProperty(type=MeleeAnim)